<?php
include('server/register/server.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Registracija</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">
</head>
<body>
    <div class="header">
        <h2>Registracija<h2>
    </div>
    <form method="post" action="register.php">
        <div class="input-group">
            <?php
            include('server/register/error.php');
            ?>
            <label>Elektroninis paštas</label><div class="required">*</div>
            <input type="email" name="email" value="">
        </div>
        <div class="input-group">
            <label>Vardas</label>
            <input type="text" name="firstname" value="">
        </div>
        <div class="input-group">
            <label>Pavardė</label>
            <input type="text" name="lastname" value="">
        </div>
        <div class="input-group">
            <label>Adresas</label>
            <input type="text" name="address" value="">
        </div>
        <div class="input-group">
            <label>Slaptažodis</label><div class="required">*</div>
            <input type="password" name="password_1" value="">
        </div>
        <div class="input-group">
            <label>Pakartokite slaptažodį</label><div class="required">*</div>
            <input type="password" name="password_2" value="">
        </div>
        <div class="input-group">
            <input type="submit" name="signup" class="button" value="Registruotis">
        </div>
    </form>
</body>
</html>
