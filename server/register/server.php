<?php
include('config.php');

$errors = 0;
$password_error = "";
$password_error2 = "";
$email_error = "";
$email_error2 = "";

// Registracija
if (isset($_POST['signup'])) {
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $firstname = mysqli_real_escape_string($connection, $_POST['firstname']);
    $lastname = mysqli_real_escape_string($connection, $_POST['lastname']);
    $address = mysqli_real_escape_string($connection, $_POST['address']);
    $password_1 = mysqli_real_escape_string($connection, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($connection, $_POST['password_2']);

    // Validacija
    if (empty($email)) {
        $email_error = "El. paštas yra privalomas";
        $errors = 1;
    }
    if (empty($password_1)) {
        $password_error = "Slaptažodis yra privalomas";
        $errors = 1;
    }
    if ($password_1 != $password_2) {
        $password_error2 = "Slaptažodžiai nesutampa";
        $errors = 1;
    }

    // Ar egzistuoja user'is
    $user_check = "SELECT * FROM users WHERE email='$email' LIMIT 1";
    $result = mysqli_query($connection, $user_check);
    $user = mysqli_fetch_assoc($result);

    if ($user) {
        if($user['email'] === $email) {
            $email_error2 = "Toks el.paštas jau egzistuoja";
            $errors = 1;
        }
    }

    if ($errors == 0) {
        $password = hash('sha256', $password_1);

        $query = "INSERT INTO `users`(`email`, `firstname`, `lastname`, `address`, `password`) VALUES ('$email', '$firstname', '$lastname', '$address', '$password')";
        $mysqli_quer = mysqli_query($connection, $query);


        $_SESSION['username'] = $username;
  	    $_SESSION['success'] = "Sėkmingai užsiregistravote.";
  	    header('location: index.php');
    }
}









?>
