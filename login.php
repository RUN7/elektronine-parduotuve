<?php include('server/login/server.php') ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Prisijungimas</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">
</head>
<body>
    <div class="header">
    	<h2>Prisijunkite</h2>
    </div>
    <form method="post" action="login.php">
        <div class="input-group">
            <?php
            include('server/login/error.php');
            ?>
            <label>El. paštas</label>
            <input type="text" name="email">
        </div>
        <div class="input-group">
            <label>Slaptažodis</label>
            <input type="password" name="password">
        </div>
        <div class="input-group">
            <input type="submit" name="login" class="button" value="Prisijungti">
        </div>
        <p>
            Dar neprisiregistravote? <a href="register.php">Užsiregistruokite</a>
        </p>
    </form>
</body>
</html>
